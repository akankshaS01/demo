import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import Contact from './components/Contact';
import About from './components/About';

const Routes = () => (
	<BrowserRouter>
		<Switch>
			<Route exact path='/' component={Home}/>
			<Route path='/contact' component={Contact}/>
			<Route path='/about' component={About}/>
		</Switch>
	</BrowserRouter>
)

export default Routes;